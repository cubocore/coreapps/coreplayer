/*
    *
    * This file is a part of CorePlayer.
    * A media player based on mpv from the CoreApps family
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <QWidget>
#include <QtWidgets>


#include <mpv/client.h>


#include <cprime/settingsmanage.h>

namespace Ui {
class coreplayer;
}

class coreplayer : public QWidget
{
    Q_OBJECT

public:
    explicit coreplayer( QWidget *parent = nullptr );
    ~coreplayer();

public Q_SLOTS:
    void play( const QString file );
    void handleMpvEvents();

    void loadFiles( QStringList paths = QStringList() );

private:
    Ui::coreplayer *ui;
    void createMPV();

    QVBoxLayout *avLyt;
    QLabel *lbl;

    QWidget *mpv_container;
    mpv_handle *mpv;

    bool mFullScreen = false;
    int mVolume = 100;
    bool mute = false;
    bool pause = false;

    void startSetup();
    void loadSettings();
    void shotcuts();

    settingsManage *sm = settingsManage::initialize();
    bool            touch;
    QSize           toolBarIconSize;
    QString         workFilePath;
    void            createPlaylist( const QString &path );
    QStringList     getList( const QString &path );

Q_SIGNALS:
    void mpv_events();

protected:
    void closeEvent( QCloseEvent *event );
	bool eventFilter( QObject *target, QEvent *event );

private slots:
    void on_playlistBtn_toggled( bool );
    void on_infoBtn_clicked();
    void on_fullscreenBtn_clicked();
    void playSelected( QListWidgetItem *item );
};
