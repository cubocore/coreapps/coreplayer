/*
    *
    * This file is a part of CorePlayer.
    * A media player based on mpv from the CoreApps family
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 2 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QApplication>
#include <QDebug>

#include "coreplayer.h"

int main ( int argc, char** argv ) {

#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(true);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CorePlayer");

    coreplayer *player = new coreplayer();
    player->show();

    QCommandLineParser parser;
    parser.addHelpOption();			// Help
    parser.addVersionOption();		// Version

	/* Optional: Path where the search begins */
    parser.addPositionalArgument( "paths", "Paths of the video files that need to be played." );

	/* Process the CLI args */
    parser.process( app );
    if ( parser.positionalArguments().count() >= 1 )
        player->loadFiles( parser.positionalArguments() );

    return app.exec();
};
